package com.techu.apitechudb.repositories;

import com.techu.apitechudb.ApitechudbApplication;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PurchaseRepository {

    public PurchaseModel save(PurchaseModel purchase) {
        System.out.println("Save en PurchaseRepository");
        ApitechudbApplication.purchaseModels.add(purchase);

        return purchase;
    }

    public List<PurchaseModel> findAll() {
        System.out.println("findAll en PurchaseRepository");

        return ApitechudbApplication.purchaseModels;
    }
    public Optional<PurchaseModel> findById(String id) {
        System.out.println("findById en PurchaseRepository");
        System.out.println("La id es " + id);

        Optional<PurchaseModel> result = Optional.empty();

        for (PurchaseModel userIntList : ApitechudbApplication.purchaseModels) {
            if (userIntList.getId().equals(id)) {
                System.out.println("Usuario encontrado con la id " + id);
                result = Optional.of(userIntList);
            }
        }
        return result;
    }
}
