package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.ProductRepository;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    UserService userService;

    @Autowired
    ProductService productService;

    public PurchaseServiceResponse addPurchase(PurchaseModel purchase) {
        System.out.println("addPurchase en PurchaseService");

        PurchaseServiceResponse result = new PurchaseServiceResponse();

        result.setPurchase(purchase);

        if (this.userService.findById(purchase.getId()).isPresent() == false) {
            System.out.println("El usuario de la compra no se ha encontrado");

            result.setMsg("El usuario de la compra no se ha encontrado");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        if (this.getById(purchase.getId()).isPresent() == true) {
            System.out.println("Ya hay una compra con esa id");

            result.setMsg("Ya hay una compra con esa id");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        float amount = 0;
        for (Map.Entry<String, Integer> purchaseItems : purchase.getPurchaseItems().entrySet()) {
            if (!this.productService.findById(purchaseItems.getKey()).isPresent()) {
                System.out.println("El producto con la id " + purchaseItems.getKey() +
                        " no se encuentra en el sistema");

            result.setMsg("El producto con la id " + purchaseItems.getKey() +
                    " no se encuentra en el sistema");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
            } else {
                System.out.println("Añadiendo valor de " + purchaseItems.getValue() + " unidades del producto al total");

                amount += this.productService.findById(purchaseItems.getKey()).get().getPrice() * purchaseItems.getValue();
            }

        }

        purchase.setAmount(amount);
        this.purchaseRepository.save(purchase);
        result.setMsg("Compra añadida correctamente");
        result.setResponseHttpStatusCode(HttpStatus.OK);

        return result;
    }

    public List<PurchaseModel> getPurchases() {
        System.out.println("getPurchases en PurchaseService");

        return this.purchaseRepository.findAll();
    }

    public Optional<PurchaseModel> getById(String id) {
        System.out.println("getById en PurchaseService");
        System.out.println("La id es " + id);

        return this.purchaseRepository.findById(id);
    }



}
